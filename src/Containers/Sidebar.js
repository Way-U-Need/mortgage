import React, { Component } from "react";

class Sidebar extends Component {
  render() {
    return (
      <div>
        <nav
          className="navbar-default navbar-static-side left-main-menu"
          role="navigation"
        >
          <div className="sidebar-collapse">
            <ul className="nav metismenu" id="side-menu">
              <li className="nav-header">
                <div className="dropdown profile-element">
                  <div className="logo text-center">
                    <a href="dashboard.html">Logo</a>
                  </div>
                </div>
                <div className="logo-element">L</div>
              </li>
              <li>
                <a href="dashboard.html">
                  <i className="fa fa-user" />
                  <span className="nav-label">User Admin</span>
                </a>
              </li>
              <li>
                <a href="payments-upcoming.html">
                  <i className="fa fa-money" />
                  <span className="nav-label">Loan Manager</span>
                </a>
              </li>
              <li>
                <a href="sessions-by-client.html">
                  <i className="fa fa-exchange" />
                  <span className="nav-label">Transfer Manager</span>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}
export default Sidebar;
