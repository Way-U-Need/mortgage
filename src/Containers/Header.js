import React, { Component } from "react";
import userImage from "../assets/images/user-img-pr.png";
class Header extends Component {
  render() {
    return (
      <div id="page-wrapper" className="gray-bg">
        <div className="row border-bottom">
          <nav
            className="navbar navbar-static-top white-bg"
            role="navigation"
            style={{ marginBottom: "0px" }}
          >
            <div className="navbar-header">
              <a
                className="navbar-minimalize minimalize-styl-2 btn btn-primary "
                href="#"
              >
                <i className="fa fa-bars" />
              </a>
              <form
                role="search"
                className="navbar-form-custom"
                action="search_results.html"
              >
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="Loan Search"
                    className="form-control"
                    name="top-search"
                    id="top-search"
                  />
                </div>
              </form>
            </div>
            <ul className="nav navbar-top-links navbar-right">
              <li className="dropdown message-header">
                <a
                  className="dropdown-toggle count-info"
                  data-toggle="dropdown"
                  href="#"
                >
                  <i className="fa fa-envelope" />
                  <span className="label label-danger">16</span>
                </a>
                <ul className="dropdown-menu dropdown-messages">
                  <li>
                    <a href="#">Messages</a>
                  </li>
                </ul>
              </li>
              <li className="dropdown notification-header">
                <a
                  className="dropdown-toggle count-info"
                  data-toggle="dropdown"
                  href="#"
                >
                  <i className="fa fa-bell" />
                  <span className="label label-danger">8</span>
                </a>
                <ul className="dropdown-menu dropdown-alerts">
                  <li>
                    <a href="#">Notification</a>
                  </li>
                </ul>
              </li>
              <li className="dropdown adm-menu">
                <a
                  className="dropdown-toggle count-info"
                  data-toggle="dropdown"
                  href="#"
                  aria-expanded="false"
                >
                  <span className="profile-img">
                    <img src={userImage} alt="img" />
                  </span>
                  <span className="hidden-xs">
                    Admin <i className="fa fa-angle-down" />
                  </span>
                </a>
                <ul className="dropdown-menu dropdown-alerts">
                  <li>
                    <a href="login.html">Logout</a>
                  </li>
                </ul>
              </li>
              <li className="dropdown advance-search-box">
                <a
                  className="dropdown-toggle count-info"
                  data-toggle="dropdown"
                  href="#"
                >
                  <i className="fa fa-ellipsis-v" aria-hidden="true" />
                </a>
                <ul className="dropdown-menu dropdown-alerts">
                  <li>
                    <input type="radio" name="Search-advance" /> Loan No.
                  </li>
                  <li>
                    <input type="radio" name="Search-advance" /> Borrower Name
                  </li>
                  <li>
                    <input type="radio" name="Search-advance" /> Property
                    address
                  </li>
                  <li>
                    <input type="radio" name="Search-advance" /> Investor
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>

        <div className="row wrapper gray-bg page-heading">
          <div className="col-lg-10 top-title-bar">
            <h2>Desktop Manager</h2>
          </div>
        </div>
        <div className="wrapper wrapper-content" />
      </div>
    );
  }
}
export default Header;
