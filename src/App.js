import React, { Component } from "react";
import Sidebar from "./Containers/Sidebar";
import Header from "./Containers/Header";

class App extends Component {
  render() {
    const wstyle = {
      paddingRight: "0px",
      paddingLeft: "0px",
      backgroundColor: "#3f51b5"
    };
    return (
      <div className="wrapper" style={wstyle}>
        <Sidebar />

        <Header />
      </div>
    );
  }
}

export default App;
